//
//  NoteTableViewCell.swift
//  noteEvent
//
//  Created by InSTEDD on 8/5/14.
//  Copyright (c) 2014 limmouyleng. All rights reserved.
//

import UIKit

class NoteTableViewCell: UITableViewCell {

    @IBOutlet var noteTitle: UILabel!
    @IBOutlet var noteDescription: UILabel!
    @IBOutlet var noteTags: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
    }
    
    required init(coder aDecoder: NSCoder!) {
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
