//
//  NoteDetailViewController.swift
//  noteEvent
//
//  Created by InSTEDD on 8/7/14.
//  Copyright (c) 2014 limmouyleng. All rights reserved.
//

import UIKit

class NoteDetailViewController: UIViewController {
    
    @IBOutlet var noteTitle: UITextField!
    @IBOutlet var noteDescription: UITextField!
    @IBOutlet var noteTags: UITextField!
    
    var note: Note!
    var delegate: NoteViewControllerDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setForm()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func noteUpdateTabbed(sender: AnyObject) {
        var note = self.toData()
        self.delegate.noteUpdateTabbed(note)
        
    }
    
    
    @IBAction func noteDeleteTabbed(sender: AnyObject) {
        println("Are you sure to delte")
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setForm() {
        self.noteTitle.text = self.note.title
        self.noteDescription.text = self.note.description
        self.noteTags.text = self.note.tagsAsString(joiner: ",")
    }
    
    func toData() -> Note {
      self.note.title = self.noteTitle.text
      self.note.description = self.noteDescription.text
      self.note.setTags(self.noteTags.text)
      return self.note
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
