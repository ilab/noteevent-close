
import Foundation

class Note {
    var id: Int  = -1
    var title: String = ""
    var description: String = ""
    var tags: Array<String> = []

    init(title:String, description:String="", tags:Array<String> = []){
        self.title = title
        self.description = description
        self.tags = tags
    }

    func setTags(tags: String) {
        var item = ""
        self.tags = []

        for charStr in tags {

           if charStr != "," {
             item += charStr
           }
           else {
             self.tags.append(item)
              item = ""
            }
        }

        if item != "" {
            self.tags.append(item)
        }
    }
    
    func tagsAsString(joiner: String = ",") -> String {
        var result = ""

        for var i=0; i < self.tags.count; i++ {
            result =  result + self.tags[i]
            if i < self.tags.count - 1 {
              result += joiner
            }
        }
        return result
        
    }
}