//
//  NoteViewController.swift
//  noteEvent
//
//  Created by limmouyleng on 7/31/14.
//  Copyright (c) 2014 limmouyleng. All rights reserved.
//

import UIKit

protocol NoteViewControllerDelegate {
    func noteDoneTabbed(note: Note)
    func noteUpdateTabbed(note: Note)
}

class NoteViewController: UIViewController {
    var delegate: NoteViewControllerDelegate!

    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var descriptionTextField: UITextField!
    @IBOutlet var tagsTextField: UITextField!
    

    @IBAction func doneTabbed(sender: AnyObject) {
        let note = self.toData()
        self.delegate.noteDoneTabbed(note)
        self.clearForm()
    }
    
    func toData()-> Note {
        let titleStr = self.titleTextField.text
        let descriptionStr = self.descriptionTextField.text

        var note = Note(title: titleStr, description: descriptionStr, tags: []);
        note.setTags(self.tagsTextField.text)
        return note
    }
    
    func clearForm() {
        self.titleTextField.text  = ""
        self.descriptionTextField.text = ""
        self.tagsTextField.text = ""
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        println(self.delegate)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    





}
