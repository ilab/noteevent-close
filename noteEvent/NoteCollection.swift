
import Foundation



class NoteCollection {
    var notes: Array<Note> = []

    init() {
        
    }
    init(note: Note) {
        notes.append(note)
    }

    func addNote(note:Note) {
        notes.append(note)
    }

    func count() -> Int {
      return notes.count
    }
    
    func update(#note: Note, atIndex: Int ) -> Bool {
        var updated = true
        self.notes.insert(note, atIndex: atIndex)
        self.notes.removeAtIndex(atIndex+1)
        return updated
    }

}