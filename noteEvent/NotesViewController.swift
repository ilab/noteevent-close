//
//  ViewController.swift
//  noteEvent
//
//  Created by limmouyleng on 7/29/14.
//  Copyright (c) 2014 limmouyleng. All rights reserved.
//

import UIKit



class NotesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NoteViewControllerDelegate{

    var noteCollection: NoteCollection = NoteCollection();
    var selectedIndexNotes: Int!
    
    @IBOutlet var noteTable: UITableView!
    
    func noteDoneTabbed(note: Note) {
        self.noteCollection.addNote(note)
        self.navigationController.popToRootViewControllerAnimated(true)
    }
    
    func noteUpdateTabbed(note: Note) {
        self.noteCollection.update(note: note, atIndex: self.selectedIndexNotes)
        self.navigationController.popToRootViewControllerAnimated(true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.noteTable.reloadData()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return noteCollection.count()
    }


    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        let cell = tableView.dequeueReusableCellWithIdentifier("note_cell", forIndexPath: indexPath) as NoteTableViewCell
        var note = self.noteCollection.notes[indexPath.row]
        cell.noteTitle.text = note.title
        cell.noteDescription.text = note.description
        cell.noteTags.text = note.tagsAsString(joiner: ", ")
        
        return cell
    }
    

    
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!){
        self.selectedIndexNotes = indexPath.row
    }

    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        

        if segue.identifier == "add_note_segue" {
            var noteVC = segue.destinationViewController as NoteViewController
            noteVC.delegate = self
        }
        else if segue.identifier == "detail_note_segue" {
          
          var clickItem = sender as NoteTableViewCell
          let titleNote = clickItem.noteTitle.text
          let descriptionNote = clickItem.noteDescription.text
          let tagsNote = clickItem.noteTags.text
          var note: Note = Note(title: titleNote, description: descriptionNote, tags: [])
    
            
          note.setTags(tagsNote)
          var noteDetail =  segue.destinationViewController as NoteDetailViewController
          
          noteDetail.note = note
          noteDetail.delegate = self

        }
    }



}

